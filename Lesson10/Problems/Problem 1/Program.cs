﻿namespace Problem_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter in settings");
            // Elektrikli avtomobil yaradılması
            ElektrikliAvtomobil tesla = new ElektrikliAvtomobil("Tesla","T3" ,85.5);

            Console.WriteLine($"Marka: {tesla.Marka}, Batareya Gücü: {tesla.BatareyaGucu} kWh");
            tesla.SesiCixar(); // Output: Bzzzzz!

            // Dizel avtomobil yaradılması
            DizelAvtomobil bmw = new DizelAvtomobil("BMW","F10", "Dizel");
            Console.WriteLine($"Marka: {bmw.Marka}, Yanacaq: {bmw.Yanacaq}");
            bmw.SesiCixar(); // Output: Vroom!


            Car2 c = new Car2();
            string[] carList = { "BMW", "Kia" };
            c.Marka = "BMW";
            c.Model = "F10";
            c.Color = "red";
            c.Year = 2018;

            int[] arr = new[] { 1, 2, 3 };

            Car2[] cars = new Car2[2];

            cars[0] = new Car2()
            {
                Marka = "BMW",
                Model = "F10",
                Color = "red",
                Year = 2018
            };

            cars[1] = new Car2()
            {
                Marka = "Kia",
                Model = "Rio",
                Color = "blcak",
                Year = 2020,
            };
            Console.WriteLine(cars[0].Marka);

            User user = new User();
            user.Name = "";
            user.LastName = "";
        }
    }

    // ElektrikliAvtomobil sınıfı yaratmaq
    class ElektrikliAvtomobil : Car
    {
        public double BatareyaGucu;

        // ElektrikliAvtomobil konstruktoru
        public ElektrikliAvtomobil(string marka, string model, double batareyaGucu) : base(marka, model)
        {
            Marka = marka;
            BatareyaGucu = batareyaGucu;
        }

        // SesiCixar metodu
        public void SesiCixar()
        {
            Console.WriteLine("Bzzzzz!");
        }
    }

    // DizelAvtomobil sınıfı yaratmaq
    class DizelAvtomobil : Car
    {
        public string Yanacaq;

        // DizelAvtomobil konstruktoru
        public DizelAvtomobil(string marka, string model, string yanacaq) : base(marka, model)
        {
            Marka = marka;
            Yanacaq = yanacaq;
        }

        // SesiCixar metodu
        public void SesiCixar()
        {
            Console.WriteLine("Vroom!");
        }
    }

    class Car
    {
        public string _color;
        public string Marka;
        public string Model;

        public Car(string marka, string model)
        {
            Marka = marka;
            Model = marka;
        }
    }

    class Car2
    {
        public int Id;
        public string Marka;
        public string Model;
        public int Year;
        public string Color;
    }

    class User
    {
        public string Name;
        public string LastName;
        public DateTime BirthDate;
        public string Email;
        public string Password;
    }
}