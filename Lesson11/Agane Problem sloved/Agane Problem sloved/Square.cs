﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agane_Problem_sloved
{
    // Kare sınıfı
    class Square : Area
    {
        private double side;

        public Square(double side)
        {
            this.side = side;
        }

        // saheni hesabla ve yaz
        public override double ShowArea()
        {
            double area = side * side;
            Console.WriteLine($"Square area: {area}");
            return area;
        }
    }
}
