﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agane_Problem_sloved
{
    class Circle : Area
    {
        public double radius;
        public double Radius { get; set; }
        public double piededi;
        public Circle(double radius, double piededi)
        {
            this.Radius = radius;
            this.piededi = piededi;
        }
        // Saheni hesabla ve yaz 
        public override double ShowArea()
        {
            double area = radius * radius * piededi;
            Console.WriteLine($"Circule area: {area}");
            return area;
        }
    }
}