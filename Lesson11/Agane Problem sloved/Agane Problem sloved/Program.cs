﻿namespace Agane_Problem_sloved
{   
    class Program
    {
        static void Main(string[] args)
        {
            // yarat
            Square square = new Square(5);
            Rectangle rectangle = new Rectangle(3, 4);
            Triangle triangle = new Triangle(3, 4, 5);
            Rob rob =new Rob(3, 6);
            Area circle = new Circle(8,3.14);
            
            // saheni ekrana  
            square.ShowArea();
            rectangle.ShowArea();
            triangle.ShowArea();
            rob.ShowArea();
            Console.WriteLine($"circle area: {circle.ShowArea()}");

            Area a = new Circle(3.4d,5.6d);
            double area =  a.ShowArea();
        }
    }
}