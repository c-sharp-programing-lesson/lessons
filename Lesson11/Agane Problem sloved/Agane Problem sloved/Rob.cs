﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agane_Problem_sloved
{
    class Rob : Area
    {
        private double length;
        private double height;

        public Rob(double length, double height)
        {
            this.length = length;
            this.height = height;
        }

        // Saheni hesabla ve yaz 
        public override double ShowArea()
        {
            double area = length * height;
            Console.WriteLine($"Rob area: {area}");
            return area;
        }
    }
}
