﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agane_Problem_sloved
{
    // Dordbucaq
    class Rectangle : Area
    {
        private double length;
        private double width;

        public Rectangle(double length, double width)
        {
            this.length = length;
            this.width = width;
        }

        // Saheni hesabla ve yaz 
        public override double ShowArea()
        {
            double area = length * width;
            Console.WriteLine($"Rectanglr area: {area}");
            return area;
        }
    }
}
