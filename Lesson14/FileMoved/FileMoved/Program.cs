﻿using System.Text;
namespace FileMoved
{
    internal class Program
    {
        private const string dowloadPath = @"C:\Download";
      
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;
            MovedFile(dowloadPath);
            Console.WriteLine("Mission compilite");
        }
        static void MovedFile(string dowloadPath)
        {
            string[] files = Directory.GetFiles(dowloadPath);
            foreach (string file in files)
            {
                string extension = Path.GetExtension(file);
                string destFolder = "";
                switch (extension)
                {
                    case ".txt":
                        destFolder = "TextFiles";
                        break;
                    case ".pptx":
                        destFolder = "PoverPointFiles";
                        break;
                    case ".pdf":
                        destFolder = "ExcelFiles";
                        break;
                    case ".docx":
                        destFolder = "WordFiles";
                        break;
                    case ".jpeg":
                    case ".png":
                        destFolder = "ImageFiles";
                        break;
                    default:
                        destFolder = "OtherFiles";
                        break;
                }
                string destPath = Path.Combine(dowloadPath, destFolder);
                if (!Directory.Exists(destPath))
                {
                    Directory.CreateDirectory(destPath);
                }
                string fileName = Path.GetFileName(file);
                string destFile = Path.Combine(destPath, fileName);
                File.Move(file,destFile);
            }
        }
    }
}