﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace fiel_create_move_read
{

    class Moved
    {
        public static void Main()
        {
            string M1 = @"C:\Users\Yuksel\Desktop\Example";
            string M2 = @"C:\Users\Yuksel\Desktop\C-Sharp-Programing-Lesson\lessons\Lesson14\C#Example";

            try
            {
                if (!File.Exists(M2))
                {
                    using (FileStream fs = File.Create(M2))
                    {
                        
                    }
                }

                if (File.Exists(M1))
                    File.Delete(M1);

                File.Move(M1,M2);
                Console.WriteLine("{0} was moved to {1}.", M1,M2);


                if (File.Exists(M1))
                {
                    Console.WriteLine("The original file still exists, which is unexpected.");
                }
                else
                {
                    Console.WriteLine("The original file no longer exists, which is expected.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
        }
    }
}