﻿using System;
using System.IO;

namespace fiel_create_move_read
{
    class Program()
    {
        private const string LMbelong =@"C:/Users/Yuksel/Desktop/C-Sharp-Programing-Lesson/lessons/Lesson14/fiel create,move,read/LM";
        static void Main()
        {
            Print();
        }
        

        #region
        static void Print()
        {
            Console.WriteLine("Enter 1 for register Student\nEnter 2 for Get Students");
            int operation = int.Parse(Console.ReadLine());

            switch (operation)
            {
                case 1:
                    CreateAndRegisterPerson();
                    break;
                case 2:
                    ShowPersonn();
                    break;
                default:
                    Console.WriteLine("Invalid operation.");
                    break;

            }
        }

        #endregion


        #region
        static void CreateAndRegisterPerson()
        {
            Person person = PreparePerson();
            if (person is not null)
                RegisterPerson(person);
            else
                Console.WriteLine("Invalid student data");
        }

        static Person PreparePerson()
        {
            Person person = new Person();
            Console.Write("Id: ");

            if (!int.TryParse(Console.ReadLine(), out int id))
            {
                Console.WriteLine("Invalid ID");
                return null;
            }
            person.Id = id;
            Console.Write("Name: ");
            person.Name = Console.ReadLine();
            Console.Write("Surname: ");
            person.Surname = Console.ReadLine();
            Console.Write("DateOfBirth: (ex: 2000.01.23): ");
            person.DateOfBirth = DateTime.Parse(Console.ReadLine());
            Console.Write("Gmail:  ");
            person.Gmail = Console.ReadLine();
            Console.Write("Where are you from country: ");
            person.Wherecountry = Console.ReadLine();
            Console.Write("Where are you from: ");
            person.Wherefrom = Console.ReadLine();
            Console.Write("Mobil numbe");
            person.MobilNuber = int.Parse(Console.ReadLine());
            return person;
        }
        static void RegisterPerson(Person person)
        {
            string fileName = $"{person.Id}.txt";
            string filePath = Path.Combine(LMbelong, fileName);
            if (!File.Exists(filePath))
            {
                string fileContent = $"Name: {person.Name}\n" +
                                     $"Surname: {person.Surname}\n" +
                                     $"DateOfBirth: {person.DateOfBirth.ToString("yyyy.MM.dd")}\n" +
                                     $"Gmail: {person.Gmail.ToString()}\n" +
                                     $"Country: {person.Wherecountry}\n" +
                                     $"City: {person.Wherefrom}\n" +
                                     $"Mobil number: {person.MobilNuber}";


                File.WriteAllText(filePath, fileContent);
                Console.WriteLine();
                Console.WriteLine("Person registered successfully.");
            }
            else
            {
                Console.WriteLine("Person already exists.");
            }
        }
        #endregion


        #region
        static Person[] GetPerson()
        {
            if (!Directory.Exists(LMbelong))
            {
                Console.WriteLine("Datebase directory not found.");
                return new Person[0];
            }
           
            string[] files = Directory.GetFiles(LMbelong, "*.txt");
            Person[] pperson = new Person[files.Length];
            for (int i = 0; i < files.Length; i++)
            {
                var fileLines = File.ReadAllLines(files[i]);
                Person person = new Person();
                person.Name = GetValueFromLine(fileLines[0]);
                person.Surname = GetValueFromLine(fileLines[1]);
                person.DateOfBirth = DateTime.Parse(GetValueFromLine(fileLines[2]));
                person.Gmail = GetValueFromLine(fileLines[3]);
                person.Wherecountry = GetValueFromLine(fileLines[4]);
                person.Wherefrom = GetValueFromLine(fileLines[5]);
                person.MobilNuber = int.Parse(GetValueFromLine(fileLines[6]));
            }
            return pperson;
        }

        static string GetValueFromLine(string line)

        {
            return line.Split(": ")[0];
        }

        static void ShowPersonn()
        {
            var pperson = GetPerson();

            if (pperson.Length == 0)
            {
                Console.WriteLine("No students found.");
                return;
            }

            foreach (var person in pperson)
            {
                ShowPerson(person);
                Console.WriteLine("-------------------------------------------------");
            }
        }

        static void ShowPerson(Person person)
        {
            Console.WriteLine($"Name: {person.Name}");
            Console.WriteLine($"Surname: {person.Surname}");
            Console.WriteLine($"DateOfBirth: {person.DateOfBirth}");
            Console.WriteLine($"Country: {person.Wherecountry}");
            Console.WriteLine($"City: {person.Wherefrom}");
            Console.WriteLine($"Mobil number:{person.MobilNuber}");
        }
        #endregion
    }
}