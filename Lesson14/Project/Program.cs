﻿using System.IO;

namespace Lesson14
{
    class Program
    {
        //C://Users/Desktop/
        private const string dbDirectory = "/home/urfan/Desktop/DB";
        static void Main()
        {
            Print();
        }

        static void Print()
        {
            Console.WriteLine("Enter 1 for register Student\nEnter 2 for Get Students");
            int operation = int.Parse(Console.ReadLine());

            switch (operation)
            {
                case 1:
                    CreateAndRegisterStudent();
                    break;
                case 2:
                    DisplayStudents();
                    break;
                default:
                    Console.WriteLine("Invalid operation.");
                    break;
            }
        }

        #region Register Student

        static void CreateAndRegisterStudent()
        {
            Student student = PrepareStudent();
            if (student is not null)
                RegisterStudent(student);
            else
                Console.WriteLine("Invalid student data");
        }
        static Student PrepareStudent()
        {
            Student student = new Student();
            Console.Write("Id: ");

            if (!int.TryParse(Console.ReadLine(), out int id))
            {
                Console.WriteLine("Invalid ID");
                return null;
            }
            student.Id = id;
            Console.Write("Name: ");
            student.Name = Console.ReadLine();
            Console.Write("Surname: ");
            student.Surname = Console.ReadLine();
            Console.Write("DateOfBirth: (ex: 2000.01.23): ");
            student.DateOfBirth = DateTime.Parse(Console.ReadLine());
            return student;
        }

        static void RegisterStudent(Student student)
        {
            string fileName = $"{student.Id}.txt";
            string filePath = Path.Combine(dbDirectory, fileName);
            if (!File.Exists(filePath))
            {
                string fileContent = $"Name: {student.Name}\n" +
                                     $"Surname: {student.Surname}\n" +
                                     $"DateOfBirth: {student.DateOfBirth.ToString("yyyy.MM.dd")}";

                File.WriteAllText(filePath, fileContent);
                Console.WriteLine();
                Console.WriteLine("Student registered successfully.");
            }
            else
            {
                Console.WriteLine("Student already exists.");
            }
        }

        #endregion

        #region Get Students

        static Student[] GetStudents()
        {
            if (!Directory.Exists(dbDirectory))
            {
                Console.WriteLine("Datebase directory not found.");
                return new Student[0];
            }

            string[] files = Directory.GetFiles(dbDirectory, "*.txt");
            Student[] students = new Student[files.Length];

            for (int i = 0; i < files.Length; i++)
            {
                var fileLines = File.ReadAllLines(files[i]);
                Student student = new Student();
                student.Name = GetValueFromLine(fileLines[0]);
                student.Surname = GetValueFromLine(fileLines[1]);
                student.DateOfBirth = DateTime.Parse(GetValueFromLine(fileLines[2]));

                students[i] = student;
            }

            return students;
        }

        static string GetValueFromLine(string line)
        {
            return line.Split(": ")[1];
        }
        
        static void DisplayStudents()
        {
            var students = GetStudents();

            if (students.Length == 0)
            {
                Console.WriteLine("No students found.");
                return;
            }

            foreach (var student in students)
            {
                DisplayStudent(student);
                Console.WriteLine("-------------------------------------------------");
            }
        }

        static void DisplayStudent(Student student)
        {
            Console.WriteLine($"Name: {student.Name}");
            Console.WriteLine($"Surname: {student.Surname}");
            Console.WriteLine($"DateOfBirth: {student.DateOfBirth}");
        }

        #endregion
    }
}