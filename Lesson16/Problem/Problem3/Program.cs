﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Problem3
{
    internal class Program
    {
        static void Main(string[] args)
        {  
            Console.WriteLine("Enter you elements:");
            EnterElemnts();
        }
        static void EnterElemnts()
        {   // iki list daxil edin
            List<Int32> Element1 = new List<Int32>();
            Element1.Add(int.Parse(Console.ReadLine()));

            List<Int32> Element2 = new List<Int32>();
            Element2.Add(int.Parse(Console.ReadLine()));

            var newList = Element1.Union(Element2);
            
            Console.WriteLine(newList);
        }
    }
}