﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Result
{

    /*
     * Complete the 'compareTriplets' function below.
     *
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY a
     *  2. INTEGER_ARRAY b
     */

    public static List<int> compareTriplets(List<int> a, List<int> b)
    {
        int firstPoint = 0;
        int secondPoint = 0;

        //int arr1= {17,28,30} = a
        //int arr2 ={99,16,30} = b
        //burda kodunu yaz.paramentr olaraq sene array gelir bes ededleri onlar daxil edir.hee aydin oldi 
        //aydindi? beli yazib atarsan  mene ini yaza bilmire cunki universtedetde miterim var ora getmeliyem //aydindi bunu kopyala at bir yere sonra yazib atarsan bura yoxlayarsanoldi.ugurlar sag olun

        //return bu olacaq buna deymersen,firstPoint,secondPoint bunlari serte gore artiracaqsan 
        if (a[0] > b[0])
            Console.WriteLine(0);
        if (a[1] >= b[1])
            Console.WriteLine(1);
        if (a[2] < b[2])
            Console.WriteLine(2);
        return new List<int>() { firstPoint, secondPoint };
    }

}

class Solution
{
    public static void Main(string[] args)
    {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        List<int> a = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(aTemp => Convert.ToInt32(aTemp)).ToList();

        List<int> b = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(bTemp => Convert.ToInt32(bTemp)).ToList();

        List<int> result = Result.compareTriplets(a, b);

        textWriter.WriteLine(String.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
