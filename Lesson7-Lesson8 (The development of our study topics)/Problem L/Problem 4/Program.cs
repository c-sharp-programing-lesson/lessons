﻿namespace Problem_4
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter the sid of the square:");
            int side = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter in symbol:");
            char symbol = char.Parse(Console.ReadLine());
            Console.WriteLine("Enter in second symbol:");
            char symbol2 = char.Parse(Console.ReadLine());
            EtrSideAndSymb(side, symbol, symbol2);
        }

        static void EtrSideAndSymb(int x, char y, char z)
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    if (i == 0 || i == x - 1 || j == 0 || j == x - 1)
                        Console.Write($"{y} ");
                    else
                        Console.Write($"{z} ");
                }
                Console.WriteLine();
            }
        }
    }
}