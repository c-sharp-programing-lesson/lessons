﻿namespace Problem_3
{
    internal class Program
    {
        static void Main(string[] args)
        {   // burda uzunluqla simvulu ozun daxil et
            Console.Write("Enter the side of the square:");
            int side = int.Parse(Console.ReadLine());
            Console.Write("Enter in symbol:");
            string symbol = Console.ReadLine();
            for (int i = 0; i < side; i++)
            {
                for (int j = 0; j < side; j++)
                {
                    Console.Write(symbol);
                }
                Console.WriteLine();
            }
        }
    }
}
