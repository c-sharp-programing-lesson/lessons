﻿namespace Problem_5
{
    internal class Program
    {
        static void Main(string[] args)
        {  //romb cek (sag sol ust alt)

            Console.WriteLine("Enter the sid of the rhombus:");
            int side = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter in symbol:");
            char symbol = char.Parse(Console.ReadLine());
            Console.WriteLine("Enter in second symbol:");
            char symbol2 = char.Parse(Console.ReadLine());
            Romb(side, symbol, symbol2);
        }
        static void Romb(int x, char y, char z)
        {
            // Romdun üst kısmını cek
            for (int i = 1; i <= x; i++)
            {
                // Sol kenarın boşluğunu Cek
                for (int j = 0; j < x - i; j++)
                {
                    Console.Write("  ");
                }

                // Romdunl kenarını cek
                for (int k = 0; k< i; k++)
                {
                    if (k ==0||k== x-1||i == x -1)
                        Console.Write(y);
                    if (k != i - 1)
                        Console.Write(" " + z + " ");
                }

                Console.WriteLine();
            }

            // Romdun alt kısmını cek
            for (int i = x - 1; i >= 1; i--)
            {
                // Sol kenarın boşluğunu cek
                for (int j = 0; j < x - i; j++)
                {
                    Console.Write("  ");
                }

                // Romdun  kenarını cek
                for (int k = 0; k < i; k++)
                {   if ( k==0|| k == x-1)
                    Console.Write(y);
                    if (k != i - 1)
                        Console.Write(" " + z + " ");
                }

                Console.WriteLine();
            }
        }
    }
}