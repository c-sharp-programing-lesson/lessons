      Bu məsələləri yazarkən hazır metodlardan istifadə elemək olmaz.
       (Məs Array.Min, Array.Max bu tipli metodlardan istifadə eləmə)

1.Verilmiş bir array-in ən böyük və ən kiçik elementini tap(max və min).
3.Bir array-də cüt ədədlərin sayını tap.
4.Verilmiş bir array-in elementlərini tərsinə çevir.
5.Bir array-də təkrarlanan elementləri tap və onları ekrana çap et.
6.Verilmiş bir array-də elementləri 2-yə böldüyünüzdə qalıqların cəmini tap.
7.Verilmiş bir array-in elementlərini sıralayan proqram yaz (artan və azalan hər ikisi).
8.Verilmiş iki array-in elementlərini toplayaraq yeni bir array yarat.

         DateTime və TimeSpan ilə bağlı suallar

1.Bir tarixin hansı gün olduğunu təyin et və ekrana yazdır (məsələn, Tarix: 01 yanvar 2024 - Gün: bazar ertəsi)
2.Bir şəxsin yaşını hesabla və 18 yaşından böyük olub olmadığını yoxla. 18 yaşından kiçikdirsə "Giriş qadağandır!" yazdır, əks halda "Giriş uğurlu oldu!" yazdır
3.Bir hadisənin başlama və bitmə tarixlərini təyin et. Sonra bu hadisənin nə qədər davam etdiyini tap və zamanı gün, saat və dəqiqə kimi ekranə yazdır.
4.Bir restoranda yemək sifarişi verildiyində, sifarişin nə zaman çatdırılacağını təyin et. Məsələn, bir nəfər 12:30-da sifariş verdi və çatdırılma müddəti 45 dəqiqədirsə, sifariş ne zaman çatdırılacaq?
5.Bir parkın iş saatlarını təyin et və bir nəfərin giriş vaxtına görə parkın açıq olub olmadığını yoxlayın
6. Bir şəxsin doğum tarixini və yaşını qəbul et. Sonra, bu şəxsin ne zaman pensiyaya çıxacağını hesabla. Azərbaycanda pensiya yaşını nəzərə al və Kişi və Qadını ayır
7.Müəyyən bir tarix aralığında neçə həftə sonu olduğunu hesabla.
8.Bir şəxsin doğum tarixini və cari yaşını götürərək, o şəxsin ne zaman 100 yaşına çatacağını tap
9.Müəyyən bir tarix aralığında neçə ədəd aprel ayı olduğunu hesabla