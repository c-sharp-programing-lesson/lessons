﻿namespace Problem_5A
{
    internal class Program
    {
        //.Bir array-də təkrarlanan elementləri tap və onları ekrana çap et.
        static void Main(string[] args)
        {
            // Define the array
            int[] arr = { 1, 2, 3, 4, 2, 5, 6, 3, 7, 8, 9, 1 };

            // Find and print duplicate elements
            FindAndPrintDuplicates(arr);
        }

        static void FindAndPrintDuplicates(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length; j++)
                {
                    if (arr[i] == arr[j])
                    {
                        Console.WriteLine(i);
                    }
                    else
                    {
                        Console.WriteLine(j);
                    }
                }
            }
        }
    }

}
