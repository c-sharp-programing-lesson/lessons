﻿namespace Problem_8A
{
    internal class Program
    {
        static void Main(string[] args)
        {  //Verilmiş iki array-in elementlərini toplayaraq yeni bir array yarat.
           // Arrays sum
            int[] arr1 = { 1, 2, 3, 4, 5 };
            int[] arr2 = { 6, 7, 8, 9, 10 };

            // Make a neü array anda them sum
            int[] newArray = new int[arr1.Length];
        for (int i = 0; i < arr1.Length; i++)
        {
            newArray[i] = arr1[i] + arr2[i];
        }

            //Skrean print
            Console.WriteLine("New Array:");
        foreach (int num in newArray)
        {
            Console.Write(num + " ");
        }
            Console.WriteLine();
            
        }
    
    }
}
