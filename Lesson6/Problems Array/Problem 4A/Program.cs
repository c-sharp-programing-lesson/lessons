﻿namespace Problem_4A
{
    internal class Program
    {

        //Verilmiş bir array-in elementlərini tərsinə çevir
        static void Main(string[] args)
        {
            // Define the array
            int[] arr = { 1, 2, 3, 4, 5 };

            // Get the reversed array
            int[] reversedArray = ReverseArray(arr);

            // Print the reversed array
            Console.WriteLine("Reversed array:");
            foreach (int num in reversedArray)
            {
                Console.Write(num + " ");
            }
        }

        static int[] ReverseArray(int[] arr)
        {
            int length = arr.Length;
            int[] reversedArray = new int[length];

            for (int i = 0; i < length; i++)
            {
                reversedArray[i] = arr[length - 1 - i];
            }

            return reversedArray;
        }
    }
}
