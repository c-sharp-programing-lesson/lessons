﻿namespace Problem_7A
{
    internal class Program
    {
        static void Main(string[] args)
        {  //Verilmiş bir array-in elementlərini sıralayan proqram yaz (artan və azalan hər ikisi).

            //Array.Sort istifade eleme
            int[] array = { 5, 2, 9, 1, 7, 3 };

            // Artan sırayla sıralama
            Console.WriteLine("Artan sıra:");
            Array.Sort(array);
            PrintArray(array);

            // Azalan sırayla sıralama
            Console.WriteLine("\nAzalan sıra:");
            Array.Reverse(array);
            PrintArray(array);
        }

        // Diziyi ekrana yazdırmak için bir fonksiyon
        static void PrintArray(int[] array)
        {
            foreach (int num in array)
            {
                Console.Write(num + " ");
            }
            Console.WriteLine();
        }
    }
}
