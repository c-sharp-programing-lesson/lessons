﻿namespace Problem_3A
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Bir array-də cüt ədədlərin sayını tap.
            {
                // Array set
                int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                // we writing pulNumbersSun funksiya call and result to  print
                Console.WriteLine("Purular numbers sum " + PulNumbersSun(arr));
            }

            static int PulNumbersSun(int[] arr)  ///
            {
                int sun = 0;
                foreach (int num in arr)
                {
                    if (num % 2 == 0)
                    {
                        sun++;
                    }
                }
                return sun;
            }
        }
    }
}
