﻿namespace Problem_5D
{
    internal class Program
    {
        static void Main(string[] args)
        {   //.Bir parkın iş saatlarını təyin et və
            //bir nəfərin giriş vaxtına görə parkın açıq olub olmadığını yoxlayın

            // Define the park's operating hours
            TimeSpan parkOpeningTime = new TimeSpan(9, 0, 0); // Park opening time (09:00 AM)
            TimeSpan parkClosingTime = new TimeSpan(18, 0, 0); // Park closing time (06:00 PM)

            // Get the user's entry time
            Console.WriteLine("Enter your entry time (HH:MM AM/PM):");
            TimeSpan entryTime = TimeSpan.Parse(Console.ReadLine());

            // Check if the park is open
            if (entryTime >= parkOpeningTime && entryTime <= parkClosingTime)
            {
                Console.WriteLine("The park is open. Enjoy your time!");
            }
            else
            {
                Console.WriteLine("Sorry, the park is closed. Please try again another time.");
            }

        }
    }
}      
