﻿namespace Problem_4D
{
    internal class Program
    {
        static void Main(string[] args)
        {

            // Specify the time the order was placed and the delivery duration (in minutes)
            TimeSpan orderTime = new TimeSpan(12, 30, 0); // 12:30
            int deliveryDurationMinutes = 45; // Delivery duration is 45 minutes

            // Calculate the time when the order will be delivered
            TimeSpan deliveryTime = orderTime.Add(new TimeSpan(0, deliveryDurationMinutes, 0));

            // Display the delivery time to the user
            Console.WriteLine("Estimated delivery time: " + deliveryTime.ToString("hh\\:mm"));

            ////Bir restoranda yemək sifarişi verildiyində, sifarişin nə zaman çatdırılacağını təyin et
            //Console.WriteLine();

            //Console.WriteLine("Tge order is ready!!!");
            //Console.WriteLine("15 minutes will be enough")


        }
         
    }
}
