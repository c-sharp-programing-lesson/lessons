﻿namespace Problem_8D
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Get the date of birth and current age of the person
            Console.WriteLine("Enter the date of birth (YYYY-MM-DD):");
            DateTime birthDate = DateTime.Parse(Console.ReadLine());

            Console.WriteLine("Enter your current age:");
            int currentAge = Convert.ToInt32(Console.ReadLine());

            // Calculate the date when the person will turn 100 years old
            DateTime dateWhenTurn100 = birthDate.AddYears(100);

            // Display the result
            Console.WriteLine("The date when you will turn 100 years old: " + dateWhenTurn100.ToString("yyyy-MM-dd"));

            //// YASI TAPMAQ VE 100 YASI NE VAXT OLACAQ
            //Console.WriteLine("Enetr you age:");
            //int age = int.Parse(Console.ReadLine());
            ////Eyni ile tarix daxil olunsun ona uygun yas hesablansin(meselen daxil olunsun 01.01.2000 indiki tarixden bunu cix hesabla yasi)
            //Console.WriteLine("That is how old you are right now==");
            //int Nowage = 2024- age;
            //Console.WriteLine(Nowage);
            //int hundrend = age + 100;
            //Console.WriteLine("You will be 100 years old in the year shown below:");
            //Console.WriteLine(hundrend);
        }
    }
}
