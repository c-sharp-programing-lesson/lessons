﻿namespace Problem_2D
{
    internal class Program
    {
        static void Main(string[] args)
        {  // Prompt the user to enter their birth date
            Console.WriteLine("Enter your birth date (YYYY-MM-DD):");
            DateTime birthDate = DateTime.Parse(Console.ReadLine());

            // Get the current date
            DateTime currentDate = DateTime.Now;

            // Calculate the user's age
            int age = currentDate.Year - birthDate.Year;

            // Check if the user hasn't had their latest birthday yet
            if (currentDate.Month < birthDate.Month ||
                (currentDate.Month == birthDate.Month && currentDate.Day < birthDate.Day))
            {
                age--;
            }

            // Check if the user is older than 18
            if (age < 18)
            {
                Console.WriteLine("Entry is prohibited!");
            }
            else
            {
                Console.WriteLine("Entry successful!");
            }
            //    // 18 yasadan asagi giris qadagandir
            //    //dogrum tarixi daxil olunsun ona uygun yas hesablansin
            //    Console.WriteLine("You Enter age:");
            //    int Age = int.Parse(Console.ReadLine());  
            //    if (Age<18)
            //    {
            //        Console.WriteLine("No entry");
            //    }
            //    else
            //    {
            //        Console.WriteLine("You can login in");
            //    }
        }
    }
}
