﻿namespace Problems_6D
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Accepting the person's date of birth and age from the user
            Console.WriteLine("Enter your date of birth (YYYY-MM-DD):");
            DateTime birthDate = DateTime.Parse(Console.ReadLine());

            Console.WriteLine("Enter your age:");
            int age = Convert.ToInt32(Console.ReadLine());

            // Retirement ages for men and women in Azerbaijan
            int maleRetirementAge = 62;
            int femaleRetirementAge = 57;

            // Determining the retirement age based on the person's gender
            int retirementAge = (age >= maleRetirementAge) ? maleRetirementAge : femaleRetirementAge;

            // Calculating the retirement date
            DateTime retirementDate = birthDate.AddYears(retirementAge);

            // Displaying the result
            Console.WriteLine("Your retirement date: " + retirementDate.ToString("yyyy-MM-dd"));

            ////  pensiya yasina ne vaxt cixacaq yeni il baximdan
            //// burda kisi ve qadini enum yaradib saxla 
            //// bu qaydada dogum tarixi daxil olunsun  DateTime startDate = Convert.ToDateTime(Console.ReadLine());
            //Console.WriteLine("Enter you gender ");
            //Console.WriteLine("Enter you birth year:");
            //Console.WriteLine("Male for 1");
            //Console.WriteLine("Famale for 2"); 
            //int age = int.Parse(Console.ReadLine());
            //string FMe = Console .ReadLine();
            //int FM;
            //FM = int.Parse(FMe);
            //int M = age + 65;
            //int F = age + 60;
            //switch  (FM)        
            //{
            //    case 1:
            //        Console.WriteLine(M);
            //        break;
            //    case 2:
            //        Console.WriteLine(F);
            //        break;

            //}
            //if (M<2024)
            //{

            //    if (F < 2024)
            //    {
            //        Console.WriteLine("You are already receiving a pension");
            //    }
            //    Console.WriteLine("You are already receiving a pension");
            //}
        }
    }
}
