﻿namespace Problem_03D
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Bir hadisənin başlama və bitmə tarixlərini təyin et.
            //Sonra bu hadisənin nə qədər davam etdiyini tap və zamanı gün, saat və dəqiqə kimi ekranə yazdır.
            Console.WriteLine("Enter the start date of the event");
            string start = Console.ReadLine();
            Console.WriteLine("Enter the finsh date of the event");
            string end = Console.ReadLine();
            DateTime startTime = DateTime.Parse(start);
            DateTime endTime = DateTime.Parse(end);
            TimeSpan result = endTime - startTime; // aradaki ferq
            //Ne qeder cekiyini yaz
            TimeSpan result1 = result *  24;
            TimeSpan result2 = result *  60;
            Console.WriteLine($"Event {result} day, {result1} hour, {result2} minute it took");
        }
    }
}
