﻿namespace Problem_9D
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the start date (YYYY-MM-DD): ");
        DateTime startDate = Convert.ToDateTime(Console.ReadLine());

        Console.WriteLine("Enter the end date (YYYY-MM-DD): ");
        DateTime endDate = Convert.ToDateTime(Console.ReadLine());

        // We will use a counter to calculate the number of April occurrences
        int aprilCount = 0;

        // We will iterate through each day between the start and end dates
        // and if any day belongs to the month of April, we will increment the counter
        for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
        {
            if (date.Month == 4) // April is represented by 4
            {
                aprilCount++;
            }
        }

        // Printing the result
        Console.WriteLine("There are " + aprilCount + " occurrences of April within the specified date range.");
        }
    }
}
