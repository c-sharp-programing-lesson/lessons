﻿namespace Problem_1D
{
    internal class Program
    {
        static void Main(string[] args)
        {   //Bir tarixin hansı gün olduğunu təyin et və ekrana yazdır 
            Console.Write("Enter in date(Format(Year/Month/Day))");
            string inPut = Console.ReadLine();

            // Tarixi strinden diger tipe cevirme
            DateTime date;
            if (DateTime.TryParse(inPut, out date))
            {
                string dayName = date.ToString("Day:"); // hansi gun oldugun teyin et
                Console.WriteLine($"{date.ToShortDateString()} Date {dayName} is day.");

            }
            else
            {
                Console.Write("You have entered the wronf format");
            }
        }
    }
}
