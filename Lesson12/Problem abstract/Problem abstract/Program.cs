﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Problem_abstract_areass;

namespace Problem_abstract
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello.Welcome in my Program...");
            Console.WriteLine("Which figure are do you wabt to calculate?");
            Console.WriteLine("These are the ones available: Circle ,Square ,Rectangular ,Rhomdus and type of Triangle ");
            Console.WriteLine("Which one you want to calculate is marked below by clicking on it:" +
                " Area of a Circle (C), Square (S), Rectangular (R), Rhomdus(X) since there is no report here as well,"
                + "by angle (H), according to thr diagonals (D),by height (B), Triangle (Z) , here too "
                + "according to the Heron formula (A), EqualTriangle (T), EquilateralTriangle (Y),RectangleTriangle (R),"
                + "Note: Each of the symbols is in capital letters.");
            Console.WriteLine();
            Console.WriteLine();
            string cr = Console.ReadLine();
            char crc = char.Parse(cr);
            Console.WriteLine();
            switch (crc)
            {
                case 'C':
                    Console.WriteLine("Enter in radius:");
                    double a = double.Parse(Console.ReadLine());
                    Circle circle = new Circle(a);
                    circle.AllSame();
                    circle.ShowArea();
                    break;
                case 'S':
                    Console.WriteLine("Enter in side:");
                    double b = double.Parse(Console.ReadLine());
                    Square square = new Square(b);
                    square.AllSame();
                    square.ShowArea();
                    break;
                case 'R':
                    Console.WriteLine("Enter in side:");
                    double c1= double.Parse(Console.ReadLine());
                    double c2= double.Parse(Console.ReadLine());
                    Rectangular rectangular = new Rectangular(c1, c2);
                    rectangular.AllSame();
                    rectangular.ShowArea();
                    break;
                case 'X':
                    Console.WriteLine("Enter in elements:");
                    float h = float.Parse(Console.ReadLine());
                    double H= double.Parse(Console.ReadLine());
                    int h1= int.Parse(Console.ReadLine());
                    int h2= int.Parse(Console.ReadLine());
                    Rhomdus rhomdus = new Rhomdus(h, H);
                    Rhomdus rhomdus2 = new Rhomdus(h1,h2);
                    Rhomdus rhomdus3 = new Rhomdus(h1,H);
                    rhomdus.AllSame();
                    rhomdus.ShowArea();
                    rhomdus2.ShowArea();
                    rhomdus3.ShowArea();
                    break;
                case 'Z':
                    Console.WriteLine("Enter in sides");
                    float z1= float.Parse(Console.ReadLine());
                    double z2= double.Parse(Console.ReadLine());
                    double z3= double.Parse(Console.ReadLine());
                    Triangle triangle = new Triangle(z3, z2, z1);
                    Triangle triangle2 = new Triangle(z2);
                    Triangle triangle3 = new Triangle(z2, z1);
                    Triangle triangle4 = new Triangle(z3, z2);
                    triangle.AllSame();
                    triangle.ShowArea();
                    triangle2.ShowArea();
                    triangle3.ShowArea();
                    triangle4.ShowArea();
                    break;
                default:
                    Console.WriteLine("You have touched the wrong icon");
                    break;
            }
        }
    }
}