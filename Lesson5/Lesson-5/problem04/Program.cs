﻿namespace problem04
{
    internal class Program
    {
        static void Main(string[] args)
        {    // vurma cedveli 
            Console.WriteLine("Enter a range for the multiplication table:");
            Console.Write("Start: ");
            int st = Convert.ToInt32(Console.ReadLine()); // neçeden başlasın

            Console.Write("Finsh: ");
            int fh = Convert.ToInt32(Console.ReadLine());//neçede bitsin

            Console.WriteLine("Multiplication table::");

            for (int s = st; s <= fh; s++)
            {
                for (int f = st; f <= fh; f++)
                {
                    int product = s * f;
                    Console.Write("{0}x{1}={2,-5}", s, f, product); // soldan saga doru getem ucun"-" isifade eledim
                }                                                   // buradi reqemlerde onun yerini bildirir
                Console.WriteLine(); // yeni sətirə keç
            }

            Console.WriteLine();// cedvel rahat qalsin sabit olaraq
        }
    
    }
}
